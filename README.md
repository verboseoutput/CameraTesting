The goal of this project is to examine the options for real time image 
processing on the android platform. Basically what percentage of a frame in
a 30 fps application is used for capturing, displaying the image versus the
time we have to actually process said image. This project started from noticing
this line in the android docs describing [the camera2 API](https://developer.android.com/reference/android/hardware/camera2/package-summary.html).

> Application-driven processing of camera data in RenderScript, OpenGL ES, or
> directly in managed or native code is best done through Allocation with a YUV 
> Type, SurfaceTexture, and ImageReader with a YUV_420_888 format, respectively.

### Native Processing
Being more familiar with C/C++ than either RenderScript or OpenGL I tried processing
the images using native code first. Aqcuiring the image is simple enough. Set an
[Image Reader's](https://developer.android.com/reference/android/media/ImageReader.html) 
surface as the target of the camera capture request. Acquiring the
latest image in Image Reader's callback gives you an android.media.Image of type
YUV_420_888 to work with. Joy of joys, the ByteBuffer of data is a direct one,
meaning no copying buffers when you pass the data to the native section of the
application.

The problems arise when you're done processing the image and wish to display the 
result to the user. You can't use a textureview because that's backed by an OpenGL
 texture which is in RGBA format. Using a SurfaceView requires creating a bitmap 
from your Image data which is very processor intensive. It is possible to use an 
ImageWriter with a reprocessable camera session. (Basically sending our manipulated 
image back through the camera hardware to output it in a more favorable format 
directly to a surface). However I wasn't able to test if I had that setup correctly 
because my phone's camera apparently doesn't support reprocessing YUV image data. 
Even if it did, I'm worried it may wreck frame rate because the same hardware has 
to both capture new image data and process old data. The most promising way I found 
to display the data efficiently was use a RenderScript to convert to an RGBA bitmap.
Which if I've reached that point, unless I have a native library I desperately 
wish to use to process the images, I feel like doing everything via render script 
makes more sense.