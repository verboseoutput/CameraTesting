package com.verboseoutput.daniel.cameratesting;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static android.content.Context.CAMERA_SERVICE;


public class CameraFragment extends Fragment {
    private static final String TAG = "CameraFragment";

    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final String FRAGMENT_DIALOG = "dialog";

    private Image mProcessImage;
    private TextureView mDisplayView;

    private String mCaptureCamera;
    private CameraDevice mCameraDevice;
    private CaptureRequest.Builder mCaptureRequestBuilder;
    private CaptureRequest mCaptureRequest;
    private CameraCaptureSession mCaptureSession;

    private ImageReader mImageReader;

    private HandlerThread mCaptureCameraBackgroundThread;
    private Handler mCaptureCameraBackgroundHandler;
    private Semaphore mCaptureCameraOpenCloseLock = new Semaphore(1);

    private HandlerThread mProcessorBackgroundThread;
    private Handler mProcessorBackgroundHandler;

    private final TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };

    /**
     * Handles changing of the camera device state
     */
    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {

            // This method is called when the camera is opened.  We start camera preview here.
            mCaptureCameraOpenCloseLock.release();
            mCameraDevice = cameraDevice;
            createCameraSession();

        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCaptureCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCaptureCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
            Activity activity = getActivity();
            if (null != activity) {
                activity.finish();
            }
        }
    };


    /**
     * Handles when a new image is available for processing in the ImageReader
     */
    private final ImageReader.OnImageAvailableListener mImageReaderListener =
            new ImageReader.OnImageAvailableListener() {

                @Override
                public void onImageAvailable(ImageReader reader) {
                    mProcessImage = reader.acquireNextImage();

                    Image.Plane[] planes = mProcessImage.getPlanes();
                    //ByteBuffer yData = planes[0].getBuffer();

                    //processRawImage(yData);

                    // TODO display processed image
                }
            };

    public CameraFragment() {
        // Required empty public constructor
    }

    public static CameraFragment newInstance() {
        CameraFragment fragment = new CameraFragment();
        // add arguments for new fragment creation here
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_camera, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDisplayView = (TextureView) view.findViewById(R.id.preview_image);
    }


    @Override
    public void onResume() {
        super.onResume();
        startBackgroundThreads();

        if(mDisplayView.isAvailable()) {
            openCamera();
        } else {
            mDisplayView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    public void onPause() {
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    private void startBackgroundThreads() {
        mCaptureCameraBackgroundThread = new HandlerThread("CameraThread");
        mCaptureCameraBackgroundThread.start();
        mCaptureCameraBackgroundHandler = new Handler(mCaptureCameraBackgroundThread.getLooper());

        mProcessorBackgroundThread = new HandlerThread("ProcessorThread");
        mProcessorBackgroundThread.start();
        mProcessorBackgroundHandler = new Handler(mProcessorBackgroundThread.getLooper());
    }

    private void stopBackgroundThread() {
        mCaptureCameraBackgroundThread.quitSafely();
        mProcessorBackgroundThread.quitSafely();
        try {
            mCaptureCameraBackgroundThread.join();
            mCaptureCameraBackgroundThread = null;
            mCaptureCameraBackgroundHandler = null;

            mProcessorBackgroundThread.join();
            mProcessorBackgroundThread = null;
            mProcessorBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Setup Camera
     * Create the Surface target where we're sending our camera data
     * Once camera is opened, calls mStateCallback
     */
    private void openCamera() {
        // Explicitly Check for Camera permissions
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
            return;
        }

        // Setup Camera and target surface
        if(setupCameraOutputs()) {
            Activity activity = getActivity();
            CameraManager manager = (CameraManager) activity.getSystemService(CAMERA_SERVICE);
            try {
                if (!mCaptureCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                    throw new RuntimeException("Time out waiting to lock camera opening.");
                }
                manager.openCamera(mCaptureCamera, mStateCallback, mCaptureCameraBackgroundHandler);

            } catch (InterruptedException e) {
                throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "Failed to setup camera outputs correctly");
        }
    }

    private void closeCamera() {
        try {
            mCaptureCameraOpenCloseLock.acquire();
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCaptureCameraOpenCloseLock.release();
        }
    }

    /**
     * @retval TRUE returns without error
     * @retval FALSE returns with error
     */
    private boolean setupCameraOutputs() {
        Activity activity = getActivity();
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);

                // We don't use a front facing camera in this application
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
                    continue;
                }

                // Map contains all of the possible stream configurations this camera supports
                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map == null) {
                    continue;
                }

                // if the camera doesn't support YUV_420_888 format we don't want it
                boolean foundYUV = Arrays.asList(map.getOutputFormats())
                        .contains(ImageFormat.YUV_420_888);

                if (!foundYUV) {
                    Log.i(TAG, "camera: " + cameraId + " does not support YUV");
                    continue;
                }

                // Looking forward to switching this to use stream, map, and min when i switch to
                // android studio 3.x
                long minDuration = Long.MAX_VALUE;
                Size optimalSize = new Size(0, 0);
                // Get the size associated with the highest frame rate (lowest duration)
                for (Size size : map.getOutputSizes(ImageFormat.YUV_420_888)) {
                    long duration = map.getOutputMinFrameDuration(ImageFormat.YUV_420_888, size);
                    if (duration < minDuration) {
                        optimalSize = size;
                        minDuration = duration;
                    }
                }
                Log.i(TAG, "optimal size: w-" + optimalSize.getWidth() + " h-" + optimalSize.getHeight());

                // Configure our target surface (in this case an ImageReader) to the optimal size for
                // frame rate
                mImageReader = ImageReader.newInstance(optimalSize.getWidth(), optimalSize.getHeight(),
                        ImageFormat.YUV_420_888, 1);
                mImageReader.setOnImageAvailableListener(mImageReaderListener, mProcessorBackgroundHandler);

                // We found a camera which meets the needs of the app so we can stop looping
                mCaptureCamera = cameraId;



                return true;
            }
            // No Camera met our requirements for this application
            ErrorDialog.newInstance(getString(R.string.no_camera_error))
                    .show(getChildFragmentManager(), FRAGMENT_DIALOG);
            return false;
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            ErrorDialog.newInstance(getString(R.string.camera_error))
                    .show(getChildFragmentManager(), FRAGMENT_DIALOG);
        }
        return false;
    }


    private void createCameraSession() {
        try {
            // This is the output Surface we're rendering raw images to
            Surface captureSurface = mImageReader.getSurface();

            // using TEMPLATE_PREVIEW because it prioritizes frame rate
            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mCaptureRequestBuilder.addTarget(captureSurface);

            // Here, we create a CameraCaptureSession for camera preview.
            mCameraDevice.createCaptureSession(Arrays.asList(captureSurface),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return;
                            }
                            // When the session is ready, we start displaying the preview.
                            mCaptureSession = cameraCaptureSession;
                            try {
                                // Auto focus should be continuous and as fast as possible
                                // TODO unsure of effect on fps
                                mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                        CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

                                // Finally, we start capturing image data
                                mCaptureRequest = mCaptureRequestBuilder.build();
                                mCaptureSession.setRepeatingRequest(mCaptureRequest,
                                        null, mCaptureCameraBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                            Log.e(TAG, "capture session configuration failed");
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void requestCameraPermission() {
        if (FragmentCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            new ConfirmationDialog().show(getChildFragmentManager(), FRAGMENT_DIALOG);
        } else {
            FragmentCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                ErrorDialog.newInstance(getString(R.string.request_permission))
                        .show(getChildFragmentManager(), FRAGMENT_DIALOG);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    /**
     * Shows an error message dialog.
     */
    public static class ErrorDialog extends DialogFragment {

        private static final String ARG_MESSAGE = "message";

        public static ErrorDialog newInstance(String message) {
            ErrorDialog dialog = new ErrorDialog();
            Bundle args = new Bundle();
            args.putString(ARG_MESSAGE, message);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Activity activity = getActivity();
            return new AlertDialog.Builder(activity)
                    .setMessage(getArguments().getString(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            activity.finish();
                        }
                    })
                    .create();
        }

    }

    /**
     * Shows OK/Cancel confirmation dialog about camera permission.
     */
    public static class ConfirmationDialog extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.request_permission)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FragmentCompat.requestPermissions(parent,
                                    new String[]{Manifest.permission.CAMERA},
                                    REQUEST_CAMERA_PERMISSION);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Activity activity = parent.getActivity();
                                    if (activity != null) {
                                        activity.finish();
                                    }
                                }
                            })
                    .create();
        }
    }

    // NATIVE FUNCTIONS

    public native void processRawImage(ByteBuffer rawImageBuff);

    static {
        System.loadLibrary("my-pixel-processor-lib");
    }
}

