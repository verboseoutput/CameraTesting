#include <jni.h>
#include <android/trace.h>

void reverseGreyScale(char* img, long size);

extern "C" {
    JNIEXPORT void JNICALL
    Java_com_verboseoutput_daniel_cameratesting_CameraFragment_processRawImage(
            JNIEnv *env,
            jobject /* this */,
            jobject rawImageBuff) {

        char* imgBuff = (char*) env->GetDirectBufferAddress(rawImageBuff);
        long imgBuffSize = env->GetDirectBufferCapacity(rawImageBuff);

        reverseGreyScale(imgBuff, imgBuffSize);
    }
}

void reverseGreyScale(char* img, long size) {
    ATrace_beginSection("reverseGreyScale");

    // Do a silly manipulation of the pixel data
    for (int i = 0; i < size; i++) {
        img[i] = 256 - img[i];
    }

    ATrace_endSection();
}